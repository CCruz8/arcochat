<?php

//create a function to retrieve the total number of matching usernames
function check_uName($uName)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE uName = :uName';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':uName', $uName);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

//create a function to retrieve the total number of matching usernames
function check_admUser($admUser)
{
  global $conn;
  $sql = 'SELECT * FROM admins WHERE admUser = :admUser';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':admUser', $admUser);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

//create a function to retrieve the total number of matching usernames
function check_linkerID($linkerID)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE memID = :memID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $linkerID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

//create a function to retrieve the total number of matching usernames
function check_linkedID($linkedID)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE memID = :memID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $linkedID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

//create a function to retrieve the total number of matching usernames
function check_memID($memID)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE memID = :memID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $memID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

//create a function to retrieve the total number of matching usernames
function check_admID($admID)
{
  global $conn;
  $sql = 'SELECT * FROM admins WHERE admID = :admID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':admID', $admID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

function check_exist_Cont($linkerID, $linkedID)
{
  global $conn;
  $sql = 'SELECT * FROM contacts WHERE linker = :linker AND linked = :linked';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':linker', $linkerID);
  $statement->bindValue(':linked', $linkedID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

function add_contact($linkerID, $linkedID)
{
  global $conn;
  $sql = "INSERT INTO contacts (linker, linked, relation) VALUES (:linkerID, :linkedID, '1')";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':linkerID', $linkerID);
  $statement->bindValue(':linkedID', $linkedID);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

function delete_contact($linkerID, $linkedID)
{
  global $conn;
  $sql = "DELETE FROM `contacts` WHERE (linker = :linkerID AND linked = :linkedID)";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':linkerID', $linkerID);
  $statement->bindValue(':linkedID', $linkedID);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

function delete_member($memID)
{
  global $conn;
  $sql = "DELETE FROM `members` WHERE memID = :memID";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $memID);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

function delete_admin($admID)
{
  global $conn;
  $sql = "DELETE FROM `admins` WHERE admID = :admID";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':admID', $admID);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

//create a function to retrieve the total number of matching usernames
function check_senderID($senderID)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE memID = :memID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $senderID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

//create a function to retrieve the total number of matching usernames
function check_receiverID($receiverID)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE memID = :memID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $receiverID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

function add_member($uName, $email, $hsdPass, $salt)
{
  global $conn;
  $sql = 'INSERT INTO members (uName, email, hsdPass, salt) VALUES (:uName, :email, :hsdPass, :salt)';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':uName', $uName);
  $statement->bindValue(':email', $email);
  $statement->bindValue(':hsdPass', $hsdPass);
  $statement->bindValue(':salt', $salt);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

function add_admin($admUser, $email, $hsdPass, $salt)
{
  global $conn;
  $sql = 'INSERT INTO admins (admUser, email, hsdPass, salt) VALUES (:admUser, :email, :hsdPass, :salt)';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':admUser', $admUser);
  $statement->bindValue(':email', $email);
  $statement->bindValue(':hsdPass', $hsdPass);
  $statement->bindValue(':salt', $salt);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

//create a function to retrieve salt
function get_adm_salt($admUser)
{
  global $conn;
  $sql = 'SELECT * FROM admins WHERE admUser = :admUser';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':admUser', $admUser);
  $statement->execute();
  $result = $statement->fetch();
  $statement->closeCursor();
  return $result;
}

//create a function to login
function adm_login($admUser, $hsdPass)
{
  global $conn;
  $sql = 'SELECT * FROM admins WHERE admUser = :admUser AND hsdPass = :hsdPass';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':admUser', $admUser);
  $statement->bindValue(':hsdPass', $hsdPass);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

//create a function to retrieve salt
function get_mem_salt($uName)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE uName = :uName';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':uName', $uName);
  $statement->execute();
  $result = $statement->fetch();
  $statement->closeCursor();
  return $result;
}

//create a function to login
function mem_login($uName, $hsdPass)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE uName = :uName AND hsdPass = :hsdPass';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':uName', $uName);
  $statement->bindValue(':hsdPass', $hsdPass);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

function get_member($memID)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE memID = :memID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $memID);
  $statement->execute();
  $result = $statement->fetch();
  $statement->closeCursor();
  return $result;
}

function get_admin($admID)
{
  global $conn;
  $sql = 'SELECT * FROM admins WHERE admID = :admID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':admID', $admID);
  $statement->execute();
  $result = $statement->fetch();
  $statement->closeCursor();
  return $result;
}

function get_member_name($memID)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE memID = :memID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $memID);
  $statement->execute();
  $result = $statement->fetch();
  $statement->closeCursor();
  return $result;
}

function get_member_ID($receiverUname)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE uName = :uName';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':uName', $receiverUname);
  $statement->execute();
  $result = $statement->fetch();
  $statement->closeCursor();
  return $result;
}

function get_members($uName,$sesUname)
{
  global $conn;
  $sql = "SELECT * FROM members WHERE (uName NOT LIKE :sesUname) AND (uName LIKE :uName) ORDER BY uName ASC";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':uName', '%' . $uName . '%');
  $statement->bindValue(':sesUname',$sesUname);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  return $result;
}

function admin_get_members($uName)
{
  global $conn;
  $sql = 'SELECT * FROM members WHERE uName LIKE :uName ORDER BY uName ASC';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':uName', '%' . $uName . '%');
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  return $result;
}

function get_all_members()
{
  global $conn;
  $sql = 'SELECT * FROM members ORDER BY uName ASC';
  $statement = $conn->prepare($sql);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  return $result;
}

//create a function to retrieve the messages based on the msgID from the url
function get_contacts($userID)
{
  global $conn;
  $sql = 'SELECT * FROM `contacts` WHERE linker = :userID ORDER BY contID ASC';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':userID', $userID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  return $result;
}

function update_member($memID, $email, $hsdPass, $salt)
{
  global $conn;
  $sql = "UPDATE members SET email = :email, hsdPass = :hsdPass, salt = :salt WHERE memID = :memID";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $memID);
  $statement->bindValue(':email', $email);
  $statement->bindValue(':hsdPass', $hsdPass);
  $statement->bindValue(':salt', $salt);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

function update_admin($admID, $email, $hsdPass, $salt)
{
  global $conn;
  $sql = "UPDATE admins SET email = :email, hsdPass = :hsdPass, salt = :salt WHERE admID = :admID";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':admID', $admID);
  $statement->bindValue(':email', $email);
  $statement->bindValue(':hsdPass', $hsdPass);
  $statement->bindValue(':salt', $salt);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

function update_member_with_photo($memID, $email, $hsdPass, $salt, $memImg)
{
  global $conn;
  $sql = "UPDATE members SET email = :email, hsdPass = :hsdPass, salt = :salt, memImg =:memImg WHERE memID = :memID";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':memID', $memID);
  $statement->bindValue(':email', $email);
  $statement->bindValue(':hsdPass', $hsdPass);
  $statement->bindValue(':salt', $salt);
  $statement->bindValue(':memImg', $memImg);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}



?>
