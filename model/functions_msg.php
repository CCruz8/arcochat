<?php

function check_exist_msg($senderID, $receiverID)
{
  global $conn;
  $sql = 'SELECT * FROM messages WHERE (senderID = :senderID AND receiverID = :receiverID) OR (senderID = :receiverID AND receiverID = :senderID)';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':senderID', $senderID);
  $statement->bindValue(':receiverID', $receiverID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

function delete_msg($senderID, $receiverID)
{
  global $conn;
  $sql = "DELETE FROM `messages` WHERE (senderID = :senderID AND receiverID = :receiverID) OR (senderID = :receiverID AND receiverID = :senderID)";
  $statement = $conn->prepare($sql);
  $statement->bindValue(':senderID', $senderID);
  $statement->bindValue(':receiverID', $receiverID);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

function add_message($senderID, $receiverID, $message, $salt)
{
  global $conn;
  $sql = 'INSERT INTO messages (senderID, receiverID, message, salt) VALUES (:senderID, :receiverID, :message, :salt)';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':senderID', $senderID);
  $statement->bindValue(':receiverID', $receiverID);
  $statement->bindValue(':message', $message);
  $statement->bindValue(':salt', $salt);
  $result = $statement->execute();
  $statement->closeCursor();
  return $result;
}

//create a function to retrieve all messages from the database sent to this user
function get_senders($receiverID)
{
  global $conn;
  $sql = 'SELECT DISTINCT senderID FROM messages WHERE receiverID = :receiverID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':receiverID', $receiverID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  return $result;
}

//create a function to retrieve all messages from the database sent to this user
function get_receivers($senderID)
{
  global $conn;
  $sql = 'SELECT DISTINCT receiverID FROM messages WHERE senderID = :senderID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':senderID', $senderID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  return $result;
}

//create a function to retrieve all messages from the database sent to this user
function get_sender($memID)
{
  global $conn;
  $sql = 'SELECT * FROM messages WHERE senderID = :senderID';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':senderID', $memID);
  $statement->execute();
  $result = $statement->fetch();
  $statement->closeCursor();
  return $result;
}

//create a function to retrieve the messages based on the msgID from the url
function get_messages($senderID, $receiverID)
{
  global $conn;
  $sql = 'SELECT * FROM `messages` WHERE (senderID = :senderID AND receiverID = :receiverID) OR (senderID = :receiverID AND receiverID = :senderID) ORDER BY msgID ASC';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':senderID', $senderID);
  $statement->bindValue(':receiverID', $receiverID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  return $result;
}

//create a function to retrieve the messages based on the msgID from the url
function sent_messages($memID)
{
  global $conn;
  $sql = 'SELECT * FROM `messages` WHERE senderID = :senderID ORDER BY msgID ASC';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':senderID', $memID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

//create a function to retrieve the messages based on the msgID from the url
function received_messages($memID)
{
  global $conn;
  $sql = 'SELECT * FROM `messages` WHERE receiverID = :receiverID ORDER BY msgID ASC';
  $statement = $conn->prepare($sql);
  $statement->bindValue(':receiverID', $memID);
  $statement->execute();
  $result = $statement->fetchAll();
  $statement->closeCursor();
  $count = $statement->rowCount();
  return $count;
}

function msg_encrypt($senderID, $receiverID, $encMessage, $salt) {

  $key = hash('sha256', $senderID.$salt);
  $iv = substr(hash('sha256', $receiverID.$salt), 0, 16);

  $message = openssl_encrypt($encMessage, "AES-256-CBC", $key, 0, $iv);
  $message = base64_encode($message);
  return $message;
}

function msg_decrypt_sender($senderID, $receiverID, $encMessage, $salt) {

  $key = hash('sha256', $receiverID.$salt);
  $iv = substr(hash('sha256', $senderID.$salt), 0, 16);

  $message = openssl_decrypt(base64_decode($encMessage), "AES-256-CBC", $key, 0, $iv);
  return $message;
}

function msg_decrypt_receiver($senderID, $receiverID, $encMessage, $salt) {

  $key = hash('sha256', $senderID.$salt);
  $iv = substr(hash('sha256', $receiverID.$salt), 0, 16);

  $message = openssl_decrypt(base64_decode($encMessage), "AES-256-CBC", $key, 0, $iv);
  return $message;
}

?>
