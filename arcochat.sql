-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2017 at 09:48 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arcochat`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admID` int(3) NOT NULL,
  `admUser` varchar(20) NOT NULL,
  `hsdPass` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admID`, `admUser`, `hsdPass`, `salt`, `email`) VALUES
(1, 'admin', '3af51491301a0bf43af70ad96e14aae83a594a85662f0791bfce4688aeff6a56', '2d2bc500c36e6774831420954bc853f4', 'rorschachi1@arco.com');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `contID` int(3) NOT NULL,
  `linker` int(3) NOT NULL,
  `linked` int(3) NOT NULL,
  `relation` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `memID` int(3) NOT NULL,
  `uName` varchar(20) NOT NULL,
  `hsdPass` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `email` varchar(100) NOT NULL,
  `memImg` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`memID`, `uName`, `hsdPass`, `salt`, `email`, `memImg`) VALUES
(1, 'admin', 'b95dc203a230e0cb21091f375f8fc2530ead090b324ec9a9503644a731c1a114', '347aa5f702d4584d9460664ca92b4e73', 'rorschachi1@arco.com', 'Nav-Finch.png'),
(2, 'reese', 'b95dc203a230e0cb21091f375f8fc2530ead090b324ec9a9503644a731c1a114', '347aa5f702d4584d9460664ca92b4e73', 'rorschachi1@arco.com', 'Nav-Reese.png'),
(4, 'shaw', 'b14ac517ed467568a224a52fb302f179c26373247a887038094c7691aa3d72e6', '440ee1dd8a53c680f7253dcd8ac5ca9c', 'shaw@mail.com', 'Nav-Shaw.png'),
(5, 'root', 'b14ac517ed467568a224a52fb302f179c26373247a887038094c7691aa3d72e6', '440ee1dd8a53c680f7253dcd8ac5ca9c', 'root@mail.com', 'Nav-Root.png'),
(6, 'bear', 'b14ac517ed467568a224a52fb302f179c26373247a887038094c7691aa3d72e6', '440ee1dd8a53c680f7253dcd8ac5ca9c', 'bear@mail.com', 'Nav-Bear.png'),
(7, 'fusco', 'b14ac517ed467568a224a52fb302f179c26373247a887038094c7691aa3d72e6', '440ee1dd8a53c680f7253dcd8ac5ca9c', 'fusco@mail.com', 'Nav-Fusco.png');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `msgID` int(3) NOT NULL,
  `senderID` int(3) NOT NULL,
  `receiverID` int(3) NOT NULL,
  `message` text NOT NULL,
  `salt` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admID`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contID`),
  ADD KEY `linker` (`linker`),
  ADD KEY `linked` (`linked`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`memID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`msgID`),
  ADD KEY `senderID` (`senderID`),
  ADD KEY `receiverID` (`receiverID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admID` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contID` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `memID` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `msgID` int(3) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`linker`) REFERENCES `members` (`memID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`linked`) REFERENCES `members` (`memID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`senderID`) REFERENCES `members` (`memID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`receiverID`) REFERENCES `members` (`memID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
