<?php
  require('../control/session_check.php');

  //retrieve sender information
  $memID = $_GET['senderID'];
  $result = get_sender($memID);
  $sender = get_member_name($memID);
?>

      <script  type="text/javascript"  src="../view/js/chat.js"></script>

      <h3>
        Chatting with: <?php echo $sender['uName'];?>
        <span class="imgCont">
          <img src="./media/target.png" height="50" width"50" class="userFG">

        <?php if (is_null($sender['memImg']) || empty($sender['memImg']) ) {
          ?>
        <img src="./media/default_img.png" alt="Default Image" width="50" height="50"><br>
          <?php
        } else {
        ?>
        <img src="./media/<?php echo $sender['memImg'];?>" alt="User's Photo Avatar" width="50" height="50"><br>
        <?php
        }?>
      </span>
      </h3>

        <div id="messageBox"><?php require('./pgAssets/msgbox.php');?></div>

        <form id="chatForm">
          <input type="hidden" name="receiverUname" id="receiverUname" value="<?php echo $sender['uName'];?>">
          <input type="hidden" name="receiver" id="receiver" value="<?php echo $_GET['senderID'];?>">
          <h3>Message*:</h3>
          <textarea class="newMsgInput form-control" rows="10"  name="message" id="message" required></textarea><br/>
          <Button id="newMsgSub" class="btn btn-lg btn-primary btn-block" type="button" disabled>Submit</button><br/>
        </form>
