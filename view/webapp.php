<?php
  require('../control/session_check.php');

  $title = 'Web App Home';
  require('./pgAssets/header.php');
  echo '<div id="alert_box"></div>';
  require('./pgAssets/nav.php');
?>

    <div id="content" class="col-md-10">
<?php
  require('./inbox.php');
?>
    </div>

<?php
  require('./pgAssets/footer.php');
?>
