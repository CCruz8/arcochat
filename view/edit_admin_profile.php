<?php
  //start the session
  session_start();

  //connect to database
  require('../model/database.php');

  //retrieve required functions

  require('../model/functions_members.php');

  $title = 'Edit Profile';
  require('./pgAssets/header.php');
  require('./pgAssets/admin_nav.php');


  $admID = $_SESSION['admID'];
  $result = get_admin($admID);
?>
      <div id="content" class="col-md-10">
        <h3>Edit Profile</h3>
        <form action="../control/edit_admin_profile_process.php" method="post" id="profileForm">
          <label for="email">Email*:</label>
          <input class="form-control" type="email" name="email" id="email"  value="<?php echo $result['email'];?>"><br/>

          <label for="password">Password*:</label>
          <input class="form-control" type="password" name="password" id="password"><br/>

          <label for="conPassword">Confirm Password*:</label>
          <input class="form-control" type="password" name="conPassword" id="conPassword"><br/>

          <input class="btn btn-lg btn-primary btn-block" type="submit"><br/>
        </form>
        <a id="regLink" href="../control/admin_delete_self.php">Delete This Administrator</a>
      </div>

<?php
  require('./pgAssets/footer.php');
?>
