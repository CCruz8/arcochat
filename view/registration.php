<?php
  //start the session
  session_start();

  //check SESSION authority
  require('../control/sign_authorisation.php');

  //connect to database
  require('../model/database.php');

  //retrieve required functions
  require('../model/functions_msg.php');


  $title = 'Registration';
  require('./pgAssets/header.php');
  echo '<div id="alert_box">';
  require('../control/messages.php');
  echo '</div>';
?>

      <script  type="text/javascript"  src="../view/js/registration.js"></script>

      <script>
      $( function() {
        var tooltips = $( "[title]" ).tooltip({
          position: {
            my: "left top",
            at: "right+5 top-5",
            collision: "none"
          }
        });
      });
      </script>

      <form  class="form-signin" action="../control/registration_process" method="post" id="signForm">
        <h2>Register to ArcoChat</h2>
        <label for="uName">Username*:</label>
        <input class="registerInput form-control" type="text" name="uName" id="uName" required autofocus title="Please Provide your preferred Username.">

        <label for="email">Email*:</label>
        <input class="registerInput form-control" type="email" name="email" id="email" required title="Please provide your email.">

        <label for="password">Password* (at least 8 characters long):</label>
        <input class="registerInput form-control" type="password" name="password" id="password" required title="Please Provide a password.">

        <label for="conPassword">Confirm Password*:</label>
        <input class="registerInput form-control" type="password" name="conPassword" id="conPassword" required title="Please confirm your password.">

        <button id="regBtn" class="btn btn-lg btn-primary btn-block" type="submit" disabled>Register</button>
        <a id="logLink" href="./login">Already have an Account? Click Me!</a>
      </form>

<?php
  require('./pgAssets/footer.php');
?>
