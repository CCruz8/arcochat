<?php
//start the session
if(!isset($_SESSION)) {
  session_start();
  //check SESSION authority
  require('../../control/authorisation.php');

  //connect to database
  require('../../model/database.php');

  //retrieve required functions
  require('../../model/functions_members.php');
  require('../../model/functions_msg.php');
}

  //retrieve sender information
  $memID = $_GET['senderID'];
  $result = get_sender($memID);
  $sender = get_member_name($memID);

  $title = 'Chatting with ' . $sender['uName'];
?>


    <?php

    //retrieve messages
    $senderID = $_GET['senderID'];
    $receiverID = $_SESSION['memID'];
    $result = get_messages($senderID, $receiverID);

    foreach ($result as $row) {

    if ($row['senderID'] == $_SESSION['memID']) {
      $encMessage = $row['message'];
      $salt = $row['salt'];
      $message = msg_decrypt_sender($senderID, $receiverID, $encMessage, $salt);
      ?>
      <p id="<?php echo $row['msgID'];?>" class="sender"><?php echo nl2br($message);?></p>
      <?php
    } else {
      $encMessage = $row['message'];
      $salt = $row['salt'];
      $message = msg_decrypt_receiver($senderID, $receiverID, $encMessage, $salt);
      ?>
      <p id="<?php echo $row['msgID'];?>" class="receiver"><?php echo nl2br($message);?></p>
      <?php
    }
  }
  ?>
