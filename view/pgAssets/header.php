<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title?> - ArcoChat</title>
<!--<link rel="icon" type="image/x-icon" href="./media/favicon.ico" />-->
<!-- google fonts -->
<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
<!-- CSS -->
<!--<link rel="stylesheet" href="./css/main.css">-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link href="./css/bootstrap-theme.css" rel="stylesheet">
<link href="./css/custom.css" rel="stylesheet">
<!-- AniCollection.css library -->
<link rel="stylesheet" href="http://anijs.github.io/lib/anicollection/anicollection.css">
<!-- js functions -->
<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>

  <script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
    integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
    crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>
  <noscript>Please Enable Javascript.</noscript>

  <header>
<?php
    if (isset($_SESSION['uName'])) {
?>
    <nav class="navbar navbar-inverse">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a id="logo" class="navbar-brand" href="./webapp">Logged in: <?php echo $_SESSION['uName'] ?></a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-wrench"></span></a>
            <ul class="dropdown-menu">
              <li><a id="profile">Edit Profile</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="../control/logout_process">Log Out</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
<?php
    } elseif (isset($_SESSION['admUser'])) {
    ?>

      <nav class="navbar navbar-inverse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a id="logo" class="navbar-brand" href="./admin_manage_members">ArcoChat &lt;Admin&gt;</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-wrench"></span></a>
              <ul class="dropdown-menu">
                <li><a href="./edit_admin_profile">Edit Profile</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="../control/admin_logout_process">Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>

      <div id="alert_box">
          <?php
          require('../control/messages.php');
          ?>
      </div>

    <?php
      } else {
?>
      <h1><a href="./login">ArcoChat</a></h1>
<?php
    }
?>
  </header>
  <div class="container-fluid">
