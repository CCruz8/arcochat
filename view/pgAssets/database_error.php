<!DOCTYPE html>
<html lang="en">

<head>
<!-- specify character encoding -->
<meta charset="utf-8" />
<!-- echo page title defined by current webpage -->
<title>Log In - ArcoChat</title>
<!-- link site icon -->
<link rel="icon" type="image/x-icon" href="./media/favicon.ico" />
<!-- link google font -->
<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
<!-- link CSS -->
<link rel="stylesheet" href="./css/main.css">
</head>

<body>
  <h1>Database Error!</h1>
</body>
</html>
