<?php
  require('../control/session_check.php');

  //retrieve members list
  $userID = $_SESSION['memID'];
  $result = get_contacts($userID);
?>
      <script  type="text/javascript"  src="../view/js/manage_conts.js"></script>

      <h3>Search Members:</h3>
      <form id="memSrcForm">
        <input class="memSrcInput" type="text" name="uName" id="uName" placeholder="e.g. JohnReese" required>
        <Button class="btn btn-lg btn-primary btn-block" id="memSrcBut" type="button" disabled>Submit</button><br/>
      </form>

      <div id="mem_cont">


        <h3>Saved Contacts:</h3>
<?php
foreach ($result as $row) {
  if ($row['linker'] == $_SESSION['memID']) {
    $memID = $row['linked'];
    $uName = get_member_name($memID);
?>
        <div id="<?php echo $memID;?>">
          <span class="imgCont">
            <img src="./media/target.png" height="50" width"50" class="userFG">
          <?php if (is_null($uName['memImg']) || empty($uName['memImg']) ) {
            ?>
            <img src="./media/default_img.png" alt="Default Image" width="50" height="50">
            <?php
          } else {
          ?>
            <img src="./media/<?php echo $uName['memImg'];?>" alt="User's Photo Avatar" width="50" height="50">
          <?php
          }?>
          </span>
          <span><?php echo $uName['uName'];?></span>
          <span><a class="message">Message</a></span>
          <span><a class="delete">Delete</a></span>
        </div>
<?php
  }
}
?>
      </div>
