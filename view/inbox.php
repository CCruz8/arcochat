<?php
  require('../control/session_check.php');

  //retrieve message senders
  $receiverID = $_SESSION['memID'];
  $result = get_senders($receiverID);

?>
      <script  type="text/javascript"  src="../view/js/inbox.js"></script>

      <h3>Inbox:</h3>
      <div id="tblHeaders" class="col-xs-12">
        <span class="col-xs-2">Avatar</span>
        <span class="col-xs-5">Username</span>
        <span class="col-xs-5">Delete</span>
      </div>
<?php
  if (!empty($result)) {

    foreach ($result as $row) {
      $memID = $row['senderID'];
      $uName = get_member_name($memID);
?>
    <div class="col-xs-12" id="<?php echo $row['senderID'];?>">
      <span class="col-xs-2 imgCont">
        <img src="./media/target.png" height="50" width"50" class="userFG">
        <?php if (is_null($uName['memImg']) || empty($uName['memImg']) ) {
          ?>
        <img src="./media/default_img.png" alt="Default Image" width="50" height="50">
          <?php
        } else {
        ?>
        <img src="./media/<?php echo $uName['memImg'];?>" alt="User's Photo Avatar" width="50" height="50">
        <?php
        }?>
      </span>

      <span class="col-xs-5"><a class="message"><?php echo $uName['uName'];?></a></span>
      <span class="col-xs-5"><a class="delete">Delete</a></span>
    </div>
<?php
    }
  } else {
?>
    <div>
      <span>Oops! Looks like your inbox is empty.</span>
    </div>
<?php
  }
?>
