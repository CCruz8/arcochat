$(document).ready( function() {
  var uName = 0;
  var password = 0;

  function checkList() {
    uName = $('#uName').val().length;
    password = $("#password").val().length;

    if (uName && password > 0) {
      $('#logBtn').prop('disabled', false);
    } else {
      $('#logBtn').prop('disabled', true);
    }
  }

  $(".loginInput").keyup(function() {
    checkList();
  });
});
