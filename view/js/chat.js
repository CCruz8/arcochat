$(document).ready(function() {
  var receiverUname = 0;
  var message = 0;

  function checkList() {
    receiverUname = $('#receiverUname').val().length;
    receiver = $('#receiver ').val().length;
    message = $('#message').val().length;

    if (message > 0) {
      $('#newMsgSub').prop('disabled', false);
    } else {
      $('#newMsgSub').prop('disabled', true);
    }
  }

  $(".newMsgInput").keyup(function() {
    checkList();
  });

    $("#message").focus();
    var inMsg = false;

    $("#messageBox").mouseenter(function() {
        inMsg = true;
    });

    $("#messageBox").mouseleave(function() {
        inMsg = false;
    });

    (function loop() {
        setTimeout(function() {
            if ($("#messageBox").length != 0) {
                if (!inMsg) {
                    var chatID = $("#receiver").val();
                    $("#messageBox").load("./pgAssets/msgbox?senderID=" + chatID);
                    var height = $("#messageBox")[0].scrollHeight;
                    $("#messageBox").stop().animate({ scrollTop: height }, 1000);
                }
            }
            loop()
        }, 15000);
    }());

    $("#message").keydown(function(event) {
        if (event.keyCode == 13 && !event.shiftKey) {
            event.preventDefault();
            var chatID = $("#receiver").val();
            $.post("../control/send_message_process", $("#chatForm").serialize(),
                function() {
                    $("#messageBox").load("./pgAssets/msgbox?senderID=" + chatID);
                    $("#alert_box").load("../control/messages");

                    var height = $("#messageBox")[0].scrollHeight;
                    $("#messageBox").stop().animate({
                        scrollTop: height
                    }, 1000);
                    $("#message").val("");
                });
        }
    });

    $("#newMsgSub").click(function() {
        var chatID = $("#receiver").val();
        $.post("../control/send_message_process", $("#chatForm").serialize(),
            function() {
                $("#messageBox").load("./pgAssets/msgbox?senderID=" + chatID);
                $("#alert_box").load("../control/messages");

                var height = $("#messageBox")[0].scrollHeight;
                $("#messageBox").stop().animate({
                    scrollTop: height
                }, 1000);
                $("#message").val("");
            });
    });

});
