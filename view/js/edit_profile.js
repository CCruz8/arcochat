$(document).ready(function () {

    $("#editPro").click(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        // Get form
        var form = $('#profileForm')[0];

		// Create an FormData object
        var data = new FormData(form);

		// disabled the submit button
        //$("#btnSubmit").prop("disabled", true);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "../control/edit_profile_process",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function(chatdata) {
              $("#content").load("./edit_profile");
              $("#alert_box").load("../control/messages");
            },
            error: function(chatdata) {
              $("#content").load("./edit_profile");
              $("#alert_box").load("../control/messages");
            },
        });

    });

});
