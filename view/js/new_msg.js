$(document).ready(function() {
  var receiverUname = 0;
  var message = 0;

  function checkList() {
    receiverUname = $('#receiverUname').val().length;
    message = $('#message').val().length;

    if (receiverUname && message > 0) {
      $('#newMsgSub').prop('disabled', false);
    } else {
      $('#newMsgSub').prop('disabled', true);
    }
  }

  $(".newMsgInput").keyup(function() {
    checkList();
  });


  $("#message").keydown(function(event) {
    if(event.keyCode == 13 && !event.shiftKey) {
      event.preventDefault();
      $.post("../control/send_message_process", $("#newMsgForm").serialize(), function(chatdata) {
        $("#content").html(chatdata);
        $("#alert_box").load("../control/messages");
        $("#message").val("");
      });
    }
  });

  $("#newMsgSub").click(function() {
    $.post("../control/send_message_process", $("#newMsgForm").serialize(), function(chatdata) {
      $("#content").html(chatdata);
      $("#alert_box").load("../control/messages");
      $("#message").val("");
    });
  });

});
