$(document).ready( function() {
  var uName = 0;
  var email = 0;
  var password = 0;
  var conPassword = 0;

  function checkList() {
    uName = $('#uName').val().length;
    email = $('#email').val().length;
    password = $("#password").val().length;
    conPassword = $("#conPassword").val().length;
    valPassword = $("#password").val();
    valConPassword = $("#conPassword").val();

    if ((uName && email >= 0) && (password && conPassword >= 8)) {
      if (valPassword === valConPassword) {
        $('#regBtn').prop('disabled', false);
      } else {
        $('#regBtn').prop('disabled', true);
      }
    } else {
      $('#regBtn').prop('disabled', true);
    }
  }

  $(".registerInput").keyup(function() {
    checkList();
  });
});
