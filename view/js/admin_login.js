$(document).ready( function() {
  var admUser = 0;
  var password = 0;

  function checkList() {
    uName = $('#admUser').val().length;
    password = $("#password").val().length;

    if (uName && password > 0) {
      $('#logBtn').prop('disabled', false);
    } else {
      $('#logBtn').prop('disabled', true);
    }
  }

  $(".loginInput").keyup(function() {
    checkList();
  });
});
