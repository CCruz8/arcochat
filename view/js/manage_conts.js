$(document).ready(function() {
  if (sessionStorage.getItem('uName')) {
    $('#uName').val(sessionStorage.getItem('uName'));
    checkList();
  } else {
    var uName = 0;
  }

  function checkList() {
    uName = $('#uName').val();

    if (uName.replace(/^\s+|\s+$/g, "").length != 0) {
      $('#memSrcBut').prop('disabled', false);
    } else {
      $('#memSrcBut').prop('disabled', true);
    }
  }

  $(".memSrcInput").keyup(function() {
    checkList();
  });

  $(".message").click(function() {
    var parentId = $(this).closest("div").attr("id");
    $("#content").load("./chat?senderID=" + parentId);
  });

  $(".delete").click(function() {
    var parentId = $(this).closest("div").attr("id");
    $.get("../control/contact_delete?delete=" + parentId, function() {
      $("#alert_box").load("../control/messages");
      $("#content").load('./manage_conts');
    });
  });


  $("#memSrcForm").keydown(function(event){
    if(event.keyCode == 13 && !event.shiftKey) {
      event.preventDefault();
        var uName = $('#uName').val();
        if ($("#uName").val() == '') {
          return false;
        } else {
          sessionStorage.setItem('uName', uName);

          $.get("../control/mems_search_json", $("#memSrcForm").serialize(), function(memData) {
            var memObj = JSON.parse(memData);
            var memIMG = "default_img.png";
            $('#mem_cont').empty();

            $('#mem_cont').append('<h3>Searched Members:</h3>' + '<div><a id="back">Go Back</a></div>');


            $.each(memObj, function() {
              if (this.memImg == '' || this.memImg == 'null') {
                memIMG = "default_img.png";
              } else {
                memIMG = this.memImg;
              }

              $('#mem_cont').append(
                '<div id="' + this.memID + '">'
                + ' <span class="imgCont">'
                + '   <img src="./media/target.png" height="50" width"50" class="userFG">'
                + '   <img src="./media/' + memIMG  + '" alt=" Photo Avatar" width="50" height="50">'
                + ' </span>'
                + '<span>' + this.uName + '</span> <span><a class="add">Add</a></span>'
                + '</div>'
              );
            });

            $(".add").click(function() {
              var parentId = $(this).closest("div").attr("id");
              $.get("../control/contact_add.php?add=" + parentId, function() {
                $("#alert_box").load("../control/messages.php");
                $("#content").load('./manage_conts.php');
              });
            });

            $("#back").click(function() {
              $("#content").load('./manage_conts.php');
            });

            $("#alert_box").load("../control/messages");
          });
        }
      }
  });

  $("#memSrcBut").click(function() {
    var uName = $('#uName').val();
    if ($("#uName").val() == '') {
      return false;
    } else {
      sessionStorage.setItem('uName', uName);

      $.get("../control/mems_search_json", $("#memSrcForm").serialize(), function(memData) {
        var memObj = JSON.parse(memData);
        var memIMG = "default_img.png";
        $('#mem_cont').empty();

        $('#mem_cont').append('<h3>Searched Members:</h3>' + '<div><a id="back">Go Back</a></div>');


        $.each(memObj, function() {
          if (this.memImg == '' || this.memImg == 'null') {
            memIMG = "default_img.png";
          } else {
            memIMG = this.memImg;
          }

          $('#mem_cont').append(
            '<div id="' + this.memID + '">'
            + ' <span class="imgCont">'
            + '   <img src="./media/target.png" height="50" width"50" class="userFG">'
            + '   <img src="./media/' + memIMG  + '" alt=" Photo Avatar" width="50" height="50">'
            + ' </span>'
            + '<span>' + this.uName + '</span> <span><a class="add">Add</a></span>'
            + '</div>'
          );
        });

        $(".add").click(function() {
          var parentId = $(this).closest("div").attr("id");
          $.get("../control/contact_add.php?add=" + parentId, function() {
            $("#alert_box").load("../control/messages.php");
            $("#content").load('./manage_conts.php');
          });
        });

        $("#back").click(function() {
          $("#content").load('./manage_conts.php');
        });

        $("#alert_box").load("../control/messages");
      });
    }
  });

  $("#uName").keyup(function(){
    var uName = $('#uName').val();
    sessionStorage.setItem('uName', uName);
  });

});
