$(document).ready(function() {

    $.ajaxPrefilter(function(options, original_Options, jqXHR) {
        options.async = true;
    });

    $("#alert_box").load("../control/messages.php");

    $("body").click(function() {
        $(".alert").alert("close");
    });
    //$("#content").load('./inbox.php');

    $('#mainNav li').click(function() {
        $('.nav li').removeClass('active');
        $(this).addClass('active');
        $(this).addClass('rubberBand animated');

        $(this).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e){
          $(this).removeClass('rubberBand animated');
        });

      });

    $("#inbox").click(function() {
        $("#content").load("./inbox.php", function() {
        });
    });

    $("#newMsg").click(function() {
        $("#content").load("./new_msg.php", function() {
            $("#receiverUname").focus();
        });
    });

    $("#manConts").click(function() {
        $("#content").load("./manage_conts.php", function() {
            $("#uName").focus();
        });
    });

    $("#outbox").click(function() {
        $("#content").load("./outbox.php");
    });

    $("#profile").click(function() {
        $("#content").load("./edit_profile.php");
    });


});
