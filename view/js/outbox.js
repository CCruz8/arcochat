$(document).ready(function() {
  $(".message").click(function() {
    var parentId = $(this).closest("div").attr("id");
    $("#content").load("./chat.php?senderID=" + parentId);
  });

  $(".delete").click(function() {
    var parentId = $(this).closest("div").attr("id");
    $.get("../control/msg_delete.php?delete=" + parentId, function() {
      $("#alert_box").load("../control/messages.php");
      $("#content").load('./outbox.php');
    });
  });
});
