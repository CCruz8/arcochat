$(document).ready(function() {
  $(".message").click(function() {
    var parentId = $(this).closest("div").attr("id");
    $("#content").load("./chat?senderID=" + parentId);
  });

  $(".delete").click(function() {
    var parentId = $(this).closest("div").attr("id");
    $.get("../control/msg_delete?delete=" + parentId, function() {
      $("#alert_box").load("../control/messages");
      $("#content").load('./inbox');
    });
  });
});
