<?php
  require('../control/session_check.php');
?>
      <script type="text/javascript" src="../view/js/new_msg.js"></script>

      <form id="newMsgForm">
        <h3>Create New Message</h3>
        <label for="receiverUname">To*:</label>
        <input class="newMsgInput form-control" type="text" name="receiverUname" id="receiverUname" placeholder="Receiver Name" required><br/>

        <label for="message">Message*:</label><br/>
        <textarea class="newMsgInput form-control" rows="10"  name="message" id="message" required></textarea><br/>
        <Button id="newMsgSub" class="btn btn-lg btn-primary btn-block" type="button" disabled>Submit</button><br/>
      </form>
