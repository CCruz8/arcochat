<?php
  require('../control/session_check.php');

  $memID = $_SESSION['memID'];
  $result = get_member($memID);
?>
      <script  type="text/javascript"  src="../view/js/edit_profile.js"></script>

      <h3>Edit Profile</h3>
      <form id="profileForm" enctype="multipart/form-data">
        <label for="memImg">Profile Picture:</label>
        <input class="form-control" type="file" name="memImg" id="memImg"><br/>
        <label for="email">Email*:</label>
        <input class="form-control" type="email" name="email" id="email"  value="<?php echo $result['email'];?>"><br/>
        <label for="password">Password*:</label>
        <input class="form-control" type="password" name="password" id="password" required><br/>
        <label for="conPassword">Confirm Password*:</label>
        <input class="form-control" type="password" name="conPassword" id="conPassword" required><br/>
        <input class="btn btn-lg btn-primary btn-block" type="button" value="Submit" id="editPro"/><br/>
      </form>
