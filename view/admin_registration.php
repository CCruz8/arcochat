<?php
  //start the session
  session_start();

  //check SESSION authority
  require('../control/admin_authorisation.php');

  //connect to database
  require('../model/database.php');

  //retrieve required functions


  $title = 'Admin Registration';
  require('./pgAssets/header.php');
  require('./pgAssets/admin_nav.php');
?>
      <div id="content" class="col-md-10">
        <h3>Register New Admin to ArcoChat</h3>
        <form action="../control/admin_registration_process" method="post" id="signForm">
          <label for="admUser">Username*:</label>
          <input class="form-control" type="text" name="admUser" id="admUser"><br/>

          <label for="email">Email*:</label>
          <input class="form-control" type="email" name="email" id="email"><br/>

          <label for="password">Password*:</label>
          <input class="form-control" type="password" name="password" id="password"><br/>

          <label for="conPassword">Confirm Password*:</label>
          <input class="form-control" type="password" name="conPassword" id="conPassword"><br/>

          <input class="btn btn-lg btn-primary btn-block" type="submit"><br/>
        </form>
      </div>

<?php
  require('./pgAssets/footer.php');
?>
