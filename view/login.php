<?php
//start the session
if(!isset($_SESSION)) {
  session_start();
  //check SESSION authority
  require('../control/sign_authorisation.php');

  //connect to database
  require('../model/database.php');

  //retrieve required functions
  require('../model/functions_members.php');
  require('../model/functions_msg.php');
}
  $title = 'Log In';
  require('./pgAssets/header.php');
  echo '<div id="alert_box">';
  require('../control/messages.php');
  echo '</div>';
?>

      <script  type="text/javascript"  src="../view/js/login.js"></script>

      <form class="form-signin" action="../control/login_process" method="post" id="signForm">
        <h2>Login to ArcoChat</h2>
        <label for="uName">Username*:</label>
        <input class="loginInput form-control" type="text" name="uName" id="uName" autofocus required>

        <label for="password">Password*:</label>
        <input class="loginInput form-control" type="password" name="password" id="password" required>
        
        <button id="logBtn" class="btn btn-lg btn-primary btn-block" type="submit" disabled>Login</button>
        <a id="regLink" href="./registration">Need an Account? Click Me!</a>
      </form>

<?php
  require('./pgAssets/footer.php');
?>
