<?php
  //start the session
  session_start();

  //check SESSION authority
  require('../control/admin_authorisation.php');

  //connect to database
  require('../model/database.php');

  //retrieve required functions
  require('../model/functions_members.php');

  require('../model/functions_msg.php');

  $title = 'Search Members';
  require('./pgAssets/header.php');
  require('./pgAssets/admin_nav.php');
?>

      <div id="content" class="col-md-10">
        <h3>Search Members:</h3>
        <form action="./admin_mems_search" method="get">
            <input type="text" name="uName" id="uName" placeholder="ex. ManInTheSuit" autofocus>
            <input class="btn btn-lg btn-primary btn-block" id="memSrcBut" type="submit"><br/>
        </form>

        <h3>Results:</h3>

        <div id="tblHeaders" class="col-xs-12">
          <span class="col-xs-3">Username</span>
          <span class="col-xs-3">Sent Messages</span>
          <span class="col-xs-3">Received Messages</span>
          <span class="col-xs-3">Delete</span>
        </div>
<?php
  //retrieve searched members list
  if (isset($_GET['uName']))
  {
    $uName = $_GET['uName'];
    if (!empty($uName))
    {
      $result = admin_get_members($uName);
      foreach ($result as $row)
      $memID = $row['memID'];
      $sent = sent_messages($memID);
      $received = received_messages($memID);
      {
?>
        <div class="col-xs-12 tblItem">
          <span class="col-xs-3" id="<?php echo $row['memID'];?>"><?php echo $row['uName'];?></span>
          <span class="col-xs-3 sent"><?php echo $sent?></span>
          <span class="col-xs-3 received"><?php echo $received?></span>
          <span class="col-xs-3"><a href="../control/admin_member_delete?delete=<?php echo $row['memID'];?>">Delete</a></span>
        </div>
<?php
      }
    }
  } else {
    echo '<div>No Searched Member/s.</div>';
  }
?>

      </div>

<?php
  require('./pgAssets/footer.php');
?>
