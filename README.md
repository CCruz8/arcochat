# README #

### Name:ArcoChat v1.0###
## Just a simple one-to-one messaging system ##

### Features: ###

  1. Encrypted messaging

  3. Contacts editing

  4. Profile editing


### Log In Details: ###
  Username: Admin

  Password: password

### Deployment Instructions: ###
  1. Go to and import './arcochat.sql' in your PHPMyAdmin.
  2. Open and edit the database details in './model/database.php' using you preferred code editor.
  3. Delete the folder '.git' if it appears in the directory. This does not affect anything for the website, this is just because the project is from a Bitbucket repository.
  3. Insert the contents of this directory to your public_html folder in your web server.
  4. Access the website through your browser.
  5. Ta Da!!!!

### Issues: ###

  1. ---

### To-do List: ###

  1. ---
