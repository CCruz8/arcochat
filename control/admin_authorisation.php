<?php
if ( (!isset($_SESSION['admID'])) && (!isset($_SESSION['admUser'])) )
{
  $_SESSION['error'] = 'Your are NOT authorised to see this page.';
  header('location:../view/admin_login');
  exit();
}
?>
