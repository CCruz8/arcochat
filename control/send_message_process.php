<?php
  require('../control/session_check.php');

	//retrieve the data of the current user using $_SESSION array and also retrieve the data posted using the $_POST array
	$senderID = strip_tags($_SESSION['memID']);
	$receiverUname = strip_tags($_POST['receiverUname']);
	$encMessage = strip_tags($_POST['message']);

	//SERVER-SIDE VALIDATION
	//check if all required fields have data
	if (empty($senderID) || empty($receiverUname) || empty($encMessage))
	{
		//if required fields are empty initialise a session called 'error' with an appropriate user message
		$_SESSION['error'] = 'All * fields are required.';
		header('location:../view/new_msg');
		exit();
	}
	else
	{
		//if all required fields HAVE data, do the following:

		//check if the senderID is NOT numeric
		if(!is_numeric($senderID))
		{
			//$senderID data is NOT numeric. do the following:
			$_SESSION['error'] = 'Please use a valid account.';
			header('location:../view/new_msg');
			exit();
		}
		else
		{
			//senderID is numeric. Check if senderID does not exist more than once in the database - count ==1
			$count = check_senderID($senderID);
			if ($count !== 1)
			{
				$_SESSION['error'] = 'Please use an existing account.';
				header('location:../view/new_msg');
				exit();
			}
		}

		$result = get_member_ID($receiverUname);
		$receiverID = $result['memID'];


		//check if the receiverID is NOT numeric
		if(!is_numeric($receiverID))
		{
			//$receiverID data is NOT numeric. do the following:
			$_SESSION['error'] = 'Receiving member is non-existent.';
			header('location:../view/new_msg');
			exit();
		}
		else
		{
			//receiverID is numeric. Check if receiverID does not exist more than once in the database - count ==1
			$count = check_receiverID($receiverID);
			if ($count !== 1)
			{
				$_SESSION['error'] = 'Receiving member does NOT exist.';
				header('location:../view/new_msg');
				exit();
			}
		}

		//END server-side validation

		if (isset($receiverID))
		{
			//encrypt the message
			$salt = md5(uniqid(rand(), true));
			$message = msg_encrypt($senderID, $receiverID, $encMessage, $salt);

			//call the add_message() function
			$result = add_message($senderID, $receiverID, $message, $salt);
			//create user messages
			if($result)
			{
				$_SESSION['success'] = 'Message created and sent successfully.';
				header("location:../view/chat?senderID=$receiverID");
				exit();
			}
			else
			{
				$_SESSION['error'] = 'An error has occurred. Please try again.';
				header('location:../view/new_msg');
				exit();
			}

		} else {
			$_SESSION['error'] = 'Member CANNOT be found. Please try again.';
			header('location:../view/new_msg');
			exit();
		}
}
?>
