<?php
session_start();

//check SESSION authority
require('../control/authorisation.php');

$result = session_destroy();
if($result)
{
  session_start();
  $_SESSION['success'] = 'Member successfully logged out.';
  header('location:../view/login');
  exit();
}
else
{
  session_start();
  $_SESSION['error'] = 'An error has occurred. Please try again.';
  header('location:../view/login');
  exit();
}
?>
