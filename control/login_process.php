<?php
	//start the session
	session_start();

	//check SESSION authority
  require('../control/sign_authorisation.php');

	//connect to database
	require('../model/database.php');

	//retrieve required functions
	require('../model/functions_members.php');

	//retrieve posted input from member_login
	$uName = strip_tags($_POST['uName']);
	$password = strip_tags($_POST['password']);

	//call the get_mem_salt() function
	$result = get_mem_salt($uName);

	//retrieve the random salt from the database
	$salt = $result['salt'];

	//generate the hashed password with the salt value
  $hsdPass = hash('sha256', $password.$salt);

	//call the login() function
	$count = mem_login($uName, $hsdPass);

	//if there is one matching record
	if($count == 1)
	{
		//start the user session to allow authorised access to secured web pages
		$_SESSION['memID'] = $result['memID'];
		$_SESSION['uName'] = $result['uName'];

		//if login is successful, create a success message to display on the products page
		$_SESSION['success'] = 'Welcome ' . $_SESSION['uName'] . '! Have a great day!';

		//redirect to webapp
		header('location:../view/webapp');
		exit();
	}
	else
	{
		//if login not successful, create an error message to display on the login page
		$_SESSION['error'] = 'Incorrect username or password. Please try again.';

		//redirect to login
		header('location:../view/login');
		exit();
	}
?>
