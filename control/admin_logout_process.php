<?php
session_start();

//check SESSION authority
require('../control/admin_authorisation.php');

$result = session_destroy();
if($result)
{
  session_start();
  $_SESSION['success'] = 'Administrator successfully logged out.';
  header('location:../view/admin_login');
  exit();
}
else
{
  session_start();
  $_SESSION['error'] = 'An error has occurred. Please try again.';
  header('location:../view/admin_login');
  exit();
}
?>
