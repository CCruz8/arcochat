admID<?php
	//start the session
	session_start();

	//check SESSION authority
	require('../control/admin_sign_authorisation.php');

	//connect to database
	require('../model/database.php');

	//retrieve required functions
	require('../model/functions_members.php');

	//retrieve posted input from member_login.php
	$admUser = strip_tags($_POST['admUser']);
	$password = strip_tags($_POST['password']);

	//call the get_mem_salt() function
	$result = get_adm_salt($admUser);

	//retrieve the random salt from the database
	$salt = $result['salt'];

	//generate the hashed password with the salt value
  $hsdPass = hash('sha256', $password.$salt);

	//call the login() function
	$count = adm_login($admUser, $hsdPass);

	//if there is one matching record
	if($count == 1)
	{
		//start the user session to allow authorised access to secured web pages
		$_SESSION['admID'] = $result['admID'];
		$_SESSION['admUser'] = $result['admUser'];

		//if login is successful, create a success message to display on the products page
		$_SESSION['success'] = 'Welcome ' . $_SESSION['admUser'] . '! Have a great day!';

		//redirect to webapp.php
		header('location:../view/admin_manage_members');
		exit();
	}
	else
	{
		//if login not successful, create an error message to display on the login page
		$_SESSION['error'] = 'Incorrect username or password. Please try again.';

		//redirect to login.php
		header('location:../view/admin_login');
		exit();
	}
?>
