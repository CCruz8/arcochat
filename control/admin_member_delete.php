<?php
	//start session management
	session_start();

	//check SESSION authority
	require('../control/admin_authorisation.php');

	//connect to the database
	require('../model/database.php');

	//retrieve the functions
	require('../model/functions_members.php');

	require('../model/functions_msg.php');

	$memID = strip_tags($_GET['delete']);

	//SERVER-SIDE VALIDATION
	//check if all required fields have data
	if (empty($memID))
	{
		//if required fields are empty initialise a session called 'error' with an appropriate user message
		$_SESSION['error'] = 'Unauthorised action.';
		header('location:../view/admin_manage_members');
		exit();
	}
	else
	{
		//if all required fields HAVE data, do the following:

		//check if the receiverID is NOT numeric
		if(!is_numeric($memID))
		{
			//$receiverID data is NOT numeric. do the following:
			$_SESSION['error'] = 'Member is invalid.';
			header('location:../view/admin_manage_members');
			exit();
		}
		else
		{
			//receiverID is numeric. Check if receiverID does not exist more than once in the database - count ==1
			$count = check_memID($memID);
			if ($count !== 1)
			{
				$_SESSION['error'] = 'Member does NOT exist.';
				header('location:../view/admin_manage_members');
				exit();
			}
		}

		//END server-side validation

		//call the add_message() function
		$result = delete_member($memID);
		//create user messages
		if($result)
		{
			$_SESSION['success'] = 'Member Deleted successfully.';
			header('location:../view/admin_manage_members');
			exit();
		}
		else
		{
			$_SESSION['error'] = 'An error has occurred. Please try again.';
			header('location:../view/admin_manage_members');
			exit();
		}
}
?>
