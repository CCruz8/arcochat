<?php
//start session management
session_start();

//check SESSION authority
require('../control/admin_authorisation.php');

//connect to the database
require('../model/database.php');

//retrieve the functions
require('../model/functions_members.php');

//retrieve the registration details into the form
$admID = strip_tags($_SESSION['admID']);
$email = strip_tags($_POST['email']);
$password = strip_tags($_POST['password']);
$conPassword = strip_tags($_POST['conPassword']);

//START server-side validation

//check if all fields have data
if (empty($admID) || empty($email) || empty($password) || empty($conPassword))
{
  //if required form fields are blank intialise a session called 'error' with an appropriate user message
  $_SESSION['error'] = 'All * fields are required.';
  //redirect to the registration page & display the message
  header('location:../view/edit_admin_profile');
  exit();
}

//check if the email is valid
elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
{
  //if the email is not valid, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Please enter a valid email address.';
  //redirect to the registration page to display the message
  header('location:../view/edit_admin_profile');
  exit();
}

//check if the password is at-least 8 characters long
elseif (strlen($password) < 8)
{
  //if password is less than 8 characters, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Password must be atleast 8 characters.';
  //redirect to the registration page & display the message
  header('location:../view/edit_admin_profile');
  exit();
}

//check if confirmation password is the same as password
elseif ($conPassword !== $password)
{
  //if password is less than 8 characters, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Confirmation password is NOT equal to password.';
  //redirect to the registration page & display the message
  header('location:../view/edit_admin_profile');
  exit();
}

//generate a random salt value using the MD5 encryption method and the PHP uniqid() and rand() functions
$salt = md5(uniqid(rand(), true));

//encrypt the password (with the concatenated salt) using the SHA256 encryption method and the PHP hash() function
$hsdPass = hash('sha256', $password.$salt); //generate the hashed password with the salt value

//END server-side validation

//START server-side account update

//call the update_member() function
$result = update_admin($admID, $email, $hsdPass, $salt);

//create user messages
if($result)
{
  //if member is successfully added, create a success message to display on the login page
  $_SESSION['success'] = 'Profile successfully edited.';
  header('location:../view/edit_admin_profile');
  exit();
}
else
{
  //if member is not successfully added, create an error message to display on the registration page
  $_SESSION['error'] = 'An error has occurred. Please try again.';
  header('location:../view/edit_admin_profile');
  exit();
}
//END server-side account creation
?>
