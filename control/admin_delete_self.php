<?php
	//start session management
	session_start();

	//check SESSION authority
	require('../control/admin_authorisation.php');

	//connect to the database
	require('../model/database.php');

	//retrieve the functions
	require('../model/functions_members.php');
	
	require('../model/functions_msg.php');

	$admID = strip_tags($_SESSION['admID']);

	//SERVER-SIDE VALIDATION
	//check if all required fields have data
	if (empty($admID))
	{
		//if required fields are empty initialise a session called 'error' with an appropriate user message
		$_SESSION['error'] = 'Unauthorised action.';
		header('location:../view/admin_edit_profile.php');
		exit();
	}
	else
	{
		//if all required fields HAVE data, do the following:

		//check if the receiverID is NOT numeric
		if(!is_numeric($admID))
		{
			//$receiverID data is NOT numeric. do the following:
			$_SESSION['error'] = 'Administrator is invalid.';
			header('location:../view/admin_edit_profile.php');
			exit();
		}
		else
		{
			//receiverID is numeric. Check if receiverID does not exist more than once in the database - count ==1
			$count = check_admID($admID);
			if ($count !== 1)
			{
				$_SESSION['error'] = 'Administrator does NOT exist.';
				header('location:../view/admin_edit_profile.php');
				exit();
			}
		}

		//END server-side validation

		//call the add_message() function
		$result = delete_admin($admID);
		//create user messages
		if($result)
		{
			$_SESSION['success'] = 'Administrator Deleted successfully.';
			header('location:../control/admin_logout_process.php');
			exit();
		}
		else
		{
			$_SESSION['error'] = 'An error has occurred. Please try again.';
			header('location:../view/admin_edit_profile.php');
			exit();
		}
}
?>
