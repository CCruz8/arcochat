<?php
  require('../control/session_check.php');

	//retrieve the data of the current user using $_SESSION array and also retrieve the data posted using the $_POST array
	$linkerID = strip_tags($_SESSION['memID']);
	$linkedID = strip_tags($_GET['add']);

	//SERVER-SIDE VALIDATION
	//check if all required fields have data
	if (empty($linkerID) || empty($linkedID))
	{
		//if required fields are empty initialise a session called 'error' with an appropriate user message
		$_SESSION['error'] = 'Unauthorised contact.';
		exit();
	}
	else
	{
		//if all required fields HAVE data, do the following:

		//check if the senderID is NOT numeric
		if(!is_numeric($linkerID))
		{
			//$senderID data is NOT numeric. do the following:
			$_SESSION['error'] = 'Please use a valid account.';
			exit();
		}
		else
		{
			//senderID is numeric. Check if senderID does not exist more than once in the database - count ==1
			$count = check_linkerID($linkerID);
			if ($count !== 1)
			{
				$_SESSION['error'] = 'Please use an existing account.';
				exit();
			}
		}

		//check if the receiverID is NOT numeric
		if(!is_numeric($linkedID))
		{
			//$receiverID data is NOT numeric. do the following:
			$_SESSION['error'] = 'Contacted member is invalid.';
			exit();
		}
		else
		{
			//receiverID is numeric. Check if receiverID does not exist more than once in the database - count ==1
			$count = check_linkedID($linkedID);
			if ($count !== 1)
			{
				$_SESSION['error'] = 'Contacted member does NOT exist.';
				exit();
			}
		}

		$count = check_exist_Cont($linkerID, $linkedID);
		if($count >= 1)
		{
			$_SESSION['error'] = 'Contacted member is already a friend.';
			exit();
		}

		//END server-side validation

		//call the add_message() function
		$result = add_contact($linkerID, $linkedID);
		//create user messages
		if($result)
		{
			$_SESSION['success'] = 'Contact added successfully.';

			exit();
		}
		else
		{
			$_SESSION['error'] = 'An error has occurred. Please try again.';
			exit();
		}
}
?>
