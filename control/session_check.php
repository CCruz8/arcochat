<?php
//start the session
if(!isset($_SESSION)) {
  session_start();
  //check SESSION authority
  require('../control/authorisation.php');

  //connect to database
  require('../model/database.php');

  //retrieve required functions
  require('../model/functions_members.php');
  require('../model/functions_msg.php');
}
?>
