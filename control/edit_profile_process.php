<?php
  require('../control/session_check.php');

//retrieve the registration details into the form
$memID = strip_tags($_SESSION['memID']);
$email = strip_tags($_POST['email']);
$password = strip_tags($_POST['password']);
$conPassword = strip_tags($_POST['conPassword']);

//START server-side validation

//check if all fields have data
if (empty($memID) || empty($email) || empty($password) || empty($conPassword))
{
  //if required form fields are blank intialise a session called 'error' with an appropriate user message
  $_SESSION['error'] = 'All * fields are required.';
  //redirect to the registration page & display the message
  exit();
}

//check if the email is valid
elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
{
  //if the email is not valid, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Please enter a valid email address.';
  //redirect to the registration page to display the message
  exit();
}

//check if the password is at-least 8 characters long
elseif (strlen($password) < 8)
{
  //if password is less than 8 characters, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Password must be atleast 8 characters.';
  //redirect to the registration page & display the message
  exit();
}

//check if confirmation password is the same as password
elseif ($conPassword !== $password)
{
  //if password is less than 8 characters, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Confirmation password is NOT equal to password.';
  //redirect to the registration page & display the message
  exit();
}

//generate a random salt value using the MD5 encryption method and the PHP uniqid() and rand() functions
$salt = md5(uniqid(rand(), true));

//encrypt the password (with the concatenated salt) using the SHA256 encryption method and the PHP hash() function
$hsdPass = hash('sha256', $password.$salt); //generate the hashed password with the salt value

//END server-side validation

//START server-side account update

if(empty($_FILES['memImg']['name'])){
  //call the update_member() function
  $result = update_member($memID, $email, $hsdPass, $salt);

  //create user messages
  if($result)
  {
    //if member is successfully added, create a success message to display on the login page
    $_SESSION['success'] = 'Profile successfully edited.';

    exit();
  }
  else
  {
    //if member is not successfully added, create an error message to display on the registration page
    $_SESSION['error'] = 'An error has occurred. Please try again.';

    exit();
  }

} else {

  $imgName = $_FILES['memImg']['name']; //the PHP file upload variable for a file
  $randomDigit = rand(0000,9999); //generate a random numerical digit <= 4 characters
  $memImg = strtolower($randomDigit . "_" . $imgName); //attach the random digit to the front of uploaded images to prevent overriding files with the same name in the images folder and enhance security
  $target = "../view/media/" . $memImg; //the target for uploaded images

  $allowedExts = array('jpg', 'jpeg', 'gif', 'png'); //create an array with the allowed file extensions
  $tmp = explode('.', $_FILES['memImg']['name']); //split the file name from the file extension
  $extension = end($tmp); //retrieve the extension of the photo e.g., png

  //check if the file is less than the maximum size of 500kb
  if($_FILES['memImg']['size'] > 512000)
  {
    //if file exceeds maximum size initialise a session called 'error' with an appropriate user message
    $_SESSION['error'] = 'Your file size exceeds maximum of 500kb.';

    exit();
  }
  //check that only accepted image formats are being uploaded
  elseif(($_FILES['memImg']['type'] == 'image/jpg') || ($_FILES['memImg']['type'] == 'image/jpeg') || ($_FILES['memImg']['type'] == 'image/gif') || ($_FILES['memImg']['type'] == 'image/png') && in_array($extension, $allowedExts))
  {
    move_uploaded_file($_FILES['memImg']['tmp_name'], $target); //move the image to images folder
  }
  else
  {
    //if a disallowed image format is uploaded initialise a session called 'error' with an appropriate user message
    $_SESSION['error'] = 'Only JPG, GIF and PNG files allowed.';

    exit();
  }


  //call the update_member() function
  $result = update_member_with_photo($memID, $email, $hsdPass, $salt, $memImg);

  //create user messages
  if($result)
  {
    //if member is successfully added, create a success message to display on the login page
    $_SESSION['success'] = 'Profile successfully edited with Image.';

    exit();
  }
  else
  {
    //if member is not successfully added, create an error message to display on the registration page
    $_SESSION['error'] = 'An error has occurred. Please try again.';

    exit();
  }
}
//END server-side account creation
?>
