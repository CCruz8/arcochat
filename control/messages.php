<?php
  require('../control/session_check.php');

		//display a user message if there is an error
		if(isset($_SESSION['error'])) {
			echo '<div id="alert_message" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>';
			echo $_SESSION['error'];
			echo '</div>';
			//unset the session named 'error' else it will show each time you visit the page
			unset($_SESSION['error']);
		}
		//display a user message if action is successful
		elseif(isset($_SESSION['success'])) {
			echo '<div id="alert_message" class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success! </strong>';
			echo $_SESSION['success'];
			echo '</div>';
			//unset the session named 'success' else it will show each time you visit the page
			unset($_SESSION['success']);
		}
?>
