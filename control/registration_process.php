<?php
//start session management
session_start();

//check SESSION authority
require('../control/sign_authorisation.php');

//connect to the database
require('../model/database.php');

//retrieve the functions
require('../model/functions_members.php');

//retrieve the registration details into the form
$uName = strip_tags($_POST['uName']);
$email = strip_tags($_POST['email']);
$password = strip_tags($_POST['password']);
$conPassword = strip_tags($_POST['conPassword']);

//START server-side validation

//check if all fields have data
if (empty($uName) || empty($email) || empty($password) || empty($conPassword))
{
  //if required form fields are blank intialise a session called 'error' with an appropriate user message
  $_SESSION['error'] = 'All * fields are required.';
  //redirect to the registration page & display the message
  header('location:../view/registration');
  exit();
}

//check if the email is valid
elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
{
  //if the email is not valid, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Please enter a valid email address.';
  //redirect to the registration page to display the message
  header('location:../view/registration');
  exit();
}

//check if the password is at-least 8 characters long
elseif (strlen($password) < 8)
{
  //if password is less than 8 characters, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Password must be atleast 8 characters.';
  //redirect to the registration page & display the message
  header('location:../view/registration');
  exit();
}

//check if confirmation password is the same as password
elseif ($conPassword !== $password)
{
  //if password is less than 8 characters, call a session 'error' with an appropriate user message
  $_SESSION['error'] = 'Confirmation password is NOT equal to password.';
  //redirect to the registration page & display the message
  header('location:../view/registration');
  exit();
}

//check if username is unavailable
$check_uName = check_uName($uName);

if ($check_uName >= 1)
{
  //if there are any matching rows intialise a session called 'error' with an appropriate user message
  $_SESSION['error'] = 'Username unavailable. Please choose a different Username.';
  //redirect to the registration page to display the message
  header('location:../view/registration');
  exit();
}

//END server-side validation

//START server-side account creation

//generate a random salt value using the MD5 encryption method and the PHP uniqid() and rand() functions
$salt = md5(uniqid(rand(), true));

//encrypt the password (with the concatenated salt) using the SHA256 encryption method and the PHP hash() function
$hsdPass = hash('sha256', $password.$salt); //generate the hashed password with the salt value

//call the add_member() function
$result = add_member($uName, $email, $hsdPass, $salt);

//create user messages
if($result)
{
  //if member is successfully added, create a success message to display on the login page
  $_SESSION['success'] = 'Thank you for creating an account. Please login.';
  //redirect to products
  header('location:../view/login');
  exit();
}
else
{
  //if member is not successfully added, create an error message to display on the registration page
  $_SESSION['error'] = 'An error has occurred. Please try again.';
  //redirect to product_add_form
  header('location:../view/registration');
  exit();
}

//END server-side account creation
?>
