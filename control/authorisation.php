<?php
if ( (!isset($_SESSION['memID'])) && (!isset($_SESSION['uName'])) )
{
  $_SESSION['error'] = 'Your are NOT authorised to see this page.';
  header('location:../view/login');
  exit();
}
?>
