<!DOCTYPE html>
<html lang="en">

<head>
<!-- specify character encoding -->
<meta charset="utf-8" />
<!-- echo page title defined by current webpage -->
<title>Log In - ArcoChat</title>
<!-- link site icon -->
<link rel="icon" type="image/x-icon" href="./media/favicon.ico" />
<!-- link google font -->
<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
<!-- link CSS -->
<link rel="stylesheet" href="./css/main.css">
<!-- link js functions -->
<script src="../view/js/main.js"></script>
</head>

<body>
  <div id="wrapper">
    <header>


      <h1><a href="./login.php">ArcoChat</a></h1>
    </header>

    <div id="content">
      <div id="cont1">
        <h3>Login to ArcoChat</h3>
        <form action="../control/login_process.php" method="post" id="signForm">
          <label for="uName">Username*:</label>
          <input type="text" name="uName" id="uName"><br/>
          <label for="password">Password*:</label>
          <input type="password" name="password" id="password"><br/>
          <div class="clearBoth"></div>
          <input type="submit" value="Log In"><br/>
        </form>
        <div class="clearBoth"></div>
        <a id="regLink" href="./registration.php">Need an Account? Click Me!</a>
        <div class="clearBoth"></div>
      </div>

    </div><!-- #content -->
    <footer>
      <p>&copy; 2016 Rorschachi. All Rights Reserved.</p>
      <p>array(0) {
}
</p>

    </footer>
  </div><!-- #wrapper -->
</body>
</html>
